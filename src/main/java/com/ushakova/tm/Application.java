package com.ushakova.tm;

import com.ushakova.tm.constant.ArgumentConst;
import com.ushakova.tm.constant.TerminalConst;
import com.ushakova.tm.model.Command;
import com.ushakova.tm.util.NumberUtil;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        final Scanner scanner = new  Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    private static void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConst.ARG_ABOUT: showAbout(); break;
            case ArgumentConst.ARG_VERSION: showVersion(); break;
            case ArgumentConst.ARG_HELP: showHelp(); break;
            case ArgumentConst.ARG_INFO: showSystemInfo(); break;
            default:
                System.out.println("Error. Argument not found!");
        }
    }

    private static void parseCommand(final String command) {
        if (command == null) return;
        switch (command) {
            case TerminalConst.CMD_ABOUT: showAbout(); break;
            case TerminalConst.CMD_VERSION: showVersion(); break;
            case TerminalConst.CMD_HELP: showHelp(); break;
            case TerminalConst.CMD_INFO: showSystemInfo(); break;
            case TerminalConst.CMD_EXIT: System.exit(0);
            default:
                System.out.println("Error. Command not found!");
        }
    }

    private static boolean parseArgs(String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg  = args[0];
        parseArg(arg);
        return true;
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Valentina Ushakova");
        System.out.println("E-MAIL: vushakova@tsconsulting.com");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(Command.ABOUT);
        System.out.println(Command.VERSION);
        System.out.println(Command.HELP);
        System.out.println(Command.INFO);
        System.out.println(Command.EXIT);
    }

    public static void showSystemInfo() {
        System.out.println("[SYSTEM INFO]");
        System.out.println("[INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " +  NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
        System.out.println("[OK]");
    }

}